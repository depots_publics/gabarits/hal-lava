\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{SelfArx2}[25/01/2012, v1.0]
\RequirePackage{ifthen}
\RequirePackage{calc}
\AtEndOfClass{\RequirePackage{microtype}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions*
\LoadClass{article}
\RequirePackage{ifpdf}      % Needed to pick between latex and pdflatex

% ---------------------------------------------------------------------
\RequirePackage[utf8]{inputenc}
\RequirePackage{amsmath}
\setlength{\mathindent}{2cm}
\RequirePackage{multicol,eso-pic}
\RequirePackage{graphicx,xcolor}
\RequirePackage[english]{babel}
\RequirePackage{booktabs}
\RequirePackage{siunitx}
\RequirePackage{array}
\RequirePackage{multirow}
\RequirePackage[super]{nth}
\RequirePackage{enumitem}  % liste description avec plus d'options
\RequirePackage{tikz} 	   % legendes figures avec lisere tikz
\RequirePackage[]{graphicx}
\RequirePackage[colorlinks]{hyperref}
\hypersetup{
 	linkcolor={rgpoly},
 	citecolor={vrpoly},
 	urlcolor={couleurdoi}
}
\RequirePackage{amssymb}
\RequirePackage{nomencl}
\RequirePackage{subfig}
% ---------------------------------------------------------------------
\newcommand{\refline}[1]{(\protect\tikz{\protect\fill[white] (0,-0.3em) rectangle (1.8em,0.3em);\protect\draw[#1,very thick] (0,0) -- (1.8em,0);})}
\newcommand{\reflinedashed}[1]{(\protect\tikz{\protect\fill[white] (0,-0.3em) rectangle (1.8em,0.3em);\protect\draw[#1,very thick,dashed] (0,0) -- (1.8em,0);})}
\newcommand{\refsquare}[1]{(\protect\tikz{\protect\fill[white] (0,-0.07em) rectangle (0.75em,0.5em);\protect\fill[#1] (0.02,-0.15em) rectangle (0.65em,.5em);})}
\newcommand{\refdot}[1]{(\protect\tikz{\protect\node[scale=0.5,solid,draw,thick,#1,circle]{};})}
\newcommand{\bm}[1]{\mathbf{#1}}
% ---------------------------------------------------------------------
\definecolor{orgpoly}{RGB}{250,150,30}
\definecolor{bleucompl}{RGB}{30,125,250}
\definecolor{rgpoly}{RGB}{185,30,50}
\definecolor{vrpoly}{RGB}{140,200,60}
\definecolor{blpoly}{RGB}{65,170,230}

\colorlet{couleurprincipale}{orgpoly}
\colorlet{couleurdoi}{black!55}
\colorlet{couleuroai}{vrpoly}

\definecolor{couleurlien}{RGB}{0,100,7}
% marges		
\RequirePackage{geometry}%
 \geometry{paperwidth=215.9mm,
  paperheight=279.4mm,
  top=100pt,
  bottom=90pt,
  textwidth=500pt,
  centering,
  headheight=12pt,
  headsep=42pt,
  footskip=30pt,
  %showframe,
  %footnotesep=24pt plus 2pt minus 12pt
 }%
\RequirePackage[labelsep=period]{caption}%
\DeclareCaptionFont{couleurprincipale}{\color{black!75}}%
\captionsetup{labelfont={couleurprincipale,bf,sf,footnotesize},font={couleurprincipale,small}}%
\captionsetup[table]{aboveskip=6pt,belowskip=0pt}%
\captionsetup[figure]{aboveskip=3pt,belowskip=0pt}%
% ---------------------------------------------------------------------
\RequirePackage{fancyhdr}  % Needed to define custom headers/footers
%\RequirePackage{lastpage}  % Number of pages in the document
\pagestyle{fancy}          % Enables the custom headers/footers
% Headers
\lhead{}%
\chead{\textcolor{black!35}{\@TitleHead}}%
\rhead{\textcolor{black!35}{}}
% Footers
\lfoot{\textcolor{black!35}{\href{mailto:\@AuthorEmail}{\@AuthorFoot}}}%
\cfoot{}%
\rfoot{\textcolor{black!35}{\thepage}}%
\renewcommand{\headrulewidth}{0.25pt}% % No header rule
\renewcommand{\footrulewidth}{0pt}% % No footer rule
\renewcommand{\headrule}{\vspace{-2mm}\hbox to\headwidth{%
  \color{orgpoly}\leaders\hrule height \headrulewidth\hfill}}
% ---------------------------------------------------------------------
% section/subsection/paragraph set-up
\RequirePackage[explicit]{titlesec}
\titleformat{\section}
  {\color{orgpoly}\bfseries\sffamily\large}
  {}
 {0em}
  {\thesection~#1}
  []
\titleformat{name=\section,numberless}
  {\color{orgpoly}\bfseries\sffamily\large}
  {}
  {0em}
  {#1}
  []
\titleformat{\subsection}
  {\color{black!75}\bfseries\sffamily}
  {\thesubsection}
  {0.5em}
  {#1}
  []
\titleformat{\subsubsection}
  {\color{black!55}\bfseries\sffamily\itshape}
  {\thesubsubsection}
  {0.5em}
  {#1}
  []
\titleformat{\paragraph}[runin]
  {\color{vrpoly}\sffamily\bfseries}
  {}
  {0em}
  {#1}
\titlespacing*{\section}{0pc}{3ex \@plus4pt \@minus3pt}{5pt}
\titlespacing*{\subsection}{0pc}{2.5ex \@plus3pt \@minus2pt}{2pt}
\titlespacing*{\subsubsection}{0pc}{2ex \@plus2.5pt \@minus1.5pt}{2pt}
\titlespacing*{\paragraph}{0pc}{1.5ex \@plus2pt \@minus1pt}{10pt}
% ---------------------------------------------------------------------
% tableofcontents set-up
\setcounter{tocdepth}{2}%
\newlength\tocsep%
\setlength{\tocsep}{.3cm}%
\usepackage{titletoc}%
\contentsmargin{0cm}%
\titlecontents{section}[\tocsep]%
  {\addvspace{2pt}\scriptsize\bfseries\sffamily}%
  {\color{couleurprincipale}\contentslabel[\thecontentslabel]{\tocsep}}%
  {}%
  {\color{couleurprincipale}\ \titlerule*[.5pc]{.}\ \thecontentspage}%
  []%
\titlecontents*{subsection}[\tocsep]%
  {\addvspace{0pt}\scriptsize\sffamily}%
  {}%
  %{\contentslabel[\thecontentslabel]{\tocsep}}
  {}%
  %{\ \titlerule*[.5pc]{.}\ \thecontentspage}
  {}%
  [\color{couleurprincipale}\;\textbullet\;]%
\titlecontents*{subsubsection}[\tocsep]%
  {\footnotesize\sffamily}%
  {}%
  {}%
  {}%
  [\textbullet]%
\titlecontents*{paragraph}[\tocsep]%
  {\footnotesize\sffamily}%
  {}%
  {}%
  {}%
  [\textbullet]%
% ---------------------------------------------------------------------
% Get the multiple author set
\newcount\@authcnt
\newcount\@tmpcnt\@tmpcnt\z@
\newcount\@authcntFr
\newcount\@tmpcntFr\@tmpcntFr\z@


\newcommand{\affiliation}[1]{\def\@affiliation{#1}}
\newcommand{\affiliationFr}[1]{\def\@affiliationFr{#1}}	
\newcommand{\affiliationb}[1]{\def\@affiliationb{#1}}
\newcommand{\affiliationFrb}[1]{\def\@affiliationFrb{#1}}	

\RequirePackage{enumitem}
\setlist{nolistsep} % Uncomment to remove spacing between items in lists (enumerate, itemize)

% numéros des références biblio dans la sections "References"
\renewcommand{\@biblabel}[1]{\bfseries\textsuperscript{[\textcolor{vrpoly}{{#1}}]}}
%\setlength{\bibitemsep}{0cm}

\newcommand{\AuthorEmail}[1]{\def\@AuthorEmail{#1}}
\newcommand{\AuthorFoot}[1]{\def\@AuthorFoot{#1}}
\newcommand{\TitleHead}[1]{\def\@TitleHead{#1}}
\newcommand{\PaperTitle}[1]{\def\@PaperTitle{#1}}
\newcommand{\PaperTitleFr}[1]{\def\@PaperTitleFr{#1}}
\newcommand{\Archive}[1]{\def\@Archive{#1}}
\newcommand{\Authors}[1]{\def\@Authors{#1}}
\newcommand{\JournalInfo}[1]{\def\@JournalInfo{#1}}
\newcommand{\Abstract}[1]{\def\@Abstract{#1}}
\newcommand{\AbstractFr}[1]{\def\@AbstractFr{#1}}
\newcommand{\Keywords}[1]{\def\@Keywords{#1}}
\newcommand{\KeywordsFr}[1]{\def\@KeywordsFr{#1}}

\renewcommand\tableofcontents{\@starttoc{toc}}
\renewcommand{\@maketitle}{%
{%
		\thispagestyle{empty}%
		%\vskip-20pt%
		\AddToShipoutPicture*{\put(48,720)%
		{\includegraphics[width=5cm]{logo/epm_logo.pdf}\hspace{10.1cm}\href{http://lava.polymtl.ca}{\includegraphics[height=1.85cm]{logo/lava-logo-complet-noir.pdf}}%
		}} 
		%{\raggedleft\small\itshape\@JournalInfo\par}%
		%\vskip20pt%
		{\color{black!75}\raggedright\Large\bfseries\sffamily\@PaperTitle\par}%
		\vskip10pt%
		{\raggedright\sffamily\large\selectfont\@Authors\par}
		\vskip18pt%
		\noindent%
		\setlength{\fboxrule}{0.25pt}%
		\setlength{\fboxsep}{3pt}%
		\fcolorbox{orgpoly}{white}
			{%
			\parbox[t]{\textwidth-2\fboxsep-2\fboxrule}{\centering%
			\colorbox{black!05}{%
				\parbox[t]{\textwidth-6\fboxsep-2\fboxrule}
					{%
					\ifx\@Keywords\@empty%
					{\small\sffamily\bfseries\textcolor{black!55}{\abstractname}}\par\@Abstract%
					\else%
					{\small\sffamily\bfseries\textcolor{black!55}{\abstractname}}\par\small\@Abstract\\[5pt]%
					{\small\sffamily\bfseries\textcolor{black!55}{\keywordname}}\par\small\@Keywords%\\[-5pt]
					%{\small\sffamily\bfseries Contents}
					%\begin{multicols}{2}
					%\tableofcontents%
					%\end{multicols}
					\fi
					}
				}%
			\vskip5pt%
			\begingroup%
				\sffamily\scriptsize\@affiliation\par%
			\endgroup}%
			}\\\vskip20pt%
%\newpage
\vfill
		{\color{black!75}\raggedright\Large\bfseries\sffamily\@PaperTitleFr\par}%
		\vskip10pt%
		{\raggedright\sffamily\large\selectfont\@Authors\par}
		\vskip18pt%
		\noindent%
		\setlength{\fboxrule}{0.25pt}%
		\setlength{\fboxsep}{3pt}%
		\fcolorbox{orgpoly}{white}
			{%
			\parbox[t]{\textwidth-2\fboxsep-2\fboxrule}{\centering%
			\colorbox{black!05}{%
				\parbox[t]{\textwidth-6\fboxsep-2\fboxrule}
					{%
					\ifx\@KeywordsFr\@empty%
					{\small\sffamily\bfseries\textcolor{black!55}{Résumé}}\par\@AbstractFr%
					\else%
					{\small\sffamily\bfseries\textcolor{black!55}{Résumé}}\par\small\@AbstractFr\\[5pt]%
					{\small\sffamily\bfseries\textcolor{black!55}{\keywordnameFr}}\par\small\@KeywordsFr%\\[-5pt]
					%{\small\sffamily\bfseries Contents}
					%\begin{multicols}{2}
					%\tableofcontents%
					%\end{multicols}
					\fi
					}
				}%
			\vskip5pt%
			\begingroup%
				\sffamily\scriptsize\@affiliationFr\par%
			\endgroup}%
			}\\\vskip20pt%	
%\newpage
\vfill			
	}%
}%
% ---------------------------------------------------------------------
\let\oldbibliography\thebibliography
\renewcommand{\thebibliography}[1]{%
\addcontentsline{toc}{section}{\hspace*{-\tocsep}\refname}%
\oldbibliography{#1}%
\setlength\itemsep{0pt}}%
\usepackage{array}
\usepackage{siunitx}
\newcommand*\ud{\mathop{}\!\mathrm{d}}
\DeclareMathOperator{\trace}{tr}
\setlength{\columnsep}{14pt}
